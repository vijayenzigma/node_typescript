import  Promise from 'promise';


export class CallbackClass{
public additionAsync(number1 : any, number2 : any , callback : Function){
        let result= 0;
    

        if(typeof number1 ==  "number" && typeof number2==  "number"){
        
            result = number1 + number2;

            callback(undefined , result);
    }
        else{
            callback('argument should be of type number', undefined);
        }
    
}

public divisionAsync(number1 : number, number2 : number){

    return new Promise(function(onResolve:any, onReject:any){

    onResolve(number1 / number2);
    onReject('argument should be of type number');
})


}
}
